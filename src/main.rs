extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    let secret_number = rand::thread_rng().gen_range(1, 101);

    //for now, it is enough to know that
    //a '!' means that we're using a macro.
    println!("Guess the number");

    loop {
        println!("Please input your guess");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = guess.trim().parse().expect("Please type in a number");

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Guess is smaller than secret number"),
            Ordering::Greater => println!("Guess is larger than secret number"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
